<?php

/**
  * RandPerm - PHP5 - generates Random Permutations.
  * Kindly provided by Jan Zaruba - http://midnightboy.oxos.net/ under GNU/GPL License
  **/ 
    
class RandomPermutations {  
  
	private static $instance;
	private $interval;
	public $init = array();
	public $in = array();
	public $a = '';
	public $b;
	public $pocet = 0;
	
  /**
   * Constructor
   */ 
	private function __construct( $interval) {
		$init =& $this->init;  
		$this->interval = $interval;
		$in = & $this->in;
		
		if (is_array($interval)) {
		  $this->a = $interval[0];
		  $this->b = $interval[1]-$interval[0]; 
		
		} else  {
			$this->b = $interval;
		}
		
		$b =& $this->b;    
		  
		for ( $i = 1; $i <= $b; $i++ ) {
			$in[]=$i;
		}
		$data = $in;
		$indexes = array();
		for ( $i = 1; $i <= $b; $i++ ) {
		       $indexes[]=ceil( lcg_value()*$in[$i-1] );
		}
		$init = $this->perm( $b, $indexes, $data  );
	}
  
  /**
   * Singleton.
   */ 
	public static function getInstance($interval)
	{
		if ( !isset( self::$instance ) || (self::$instance->interval!=$interval)) {
		  $i = __CLASS__;
		  self::$instance = new $i($interval);
		}
		return self::$instance;
	}
  
  
  
	public function perm($n, $indexes, $data) {
		for ($i = $n; $i > 0; $i--) {
		  $indexValue = $indexes[$i - 1];
		  $dataNumber = $data[$i - 1];
		  $data[$i - 1] = $data[$indexValue - 1];
		  $data[$indexValue - 1] = $dataNumber;
		}
		return $data;  
	}
 
	public function getValues($isPreindexace = false) {
		$a =& $this->a;
		$b =& $this->b;
		$init =& $this->init;
		$in =& $this->in;
		
		$indexes = array();
		for ($i = 1; $i <= $b; $i++) {
			$indexes[]= ceil(lcg_value() * $in[$i - 1]);
		} 
		
		$init = $this->perm($b, $indexes, $init);
		$preindexace = array();
		for ($i = 1; $i <= $b; $i++) {
		  $init[$i - 1] = ((empty($a)) ? $init[$i - 1] : $init[$i - 1] + $a);
		  $preindexace[$i] = $init[$i - 1];
		}

		if ($isPreindexace) {
			return $this->validateValues($preindexace);
		}
    else {
			return $this->validateValues($init);
		}
	}
	
	public function validateValues(&$permutace) {
		for ($i = 1; $i <= $this->b; $i++) {
		    for ($j = $i; $j <= $this->b; $j++) {
			    if ($j != $i) {
			          if ($permutace[$i] == $permutace[$j]) {
			            return false;
			          }
			    }
		    }
		}
		return $permutace;    
	} 

}

function rand_permute($size, $min, $max) {
    $retval = array();
    //initialize an array of integers from $min to $max
    for($i = $min;$i <= $max;$i++)
    {
        $retval[$i] = $i;
    }
    //start with the the first index ($min).
    //randomly swap this number with any other number in the array.
    //this way we guarantee all numbers are permuted in the array,
    //and we assure no number is used more than once (technically reiterating prev line).
    //therefore we don't have to do the random checking each time we put something into the array.
    for($i=$min; $i < $size; $i++)
    {
        $tmp = $retval[$i];
        $retval[$i] = $retval[$tmpkey = rand($min, $max)];
        $retval[$tmpkey] = $tmp;
    }
    return array_slice($retval, 0, $size);
}

/*
andom numbers with Gauss distribution (normal distribution).
A correct alghoritm. Without aproximations, like Smaaps'
It is specially usefull for simulations in physics.
Check yourself, and have a fun.
*/

function gauss()
{   // N(0,1)
    // returns random number with normal distribution:
    //   mean=0
    //   std dev=1
   
    // auxilary vars
    $x=random_0_1();
    $y=random_0_1();
   
    // two independent variables with normal distribution N(0,1)
    $u=sqrt(-2*log($x))*cos(2*pi()*$y);
    $v=sqrt(-2*log($x))*sin(2*pi()*$y);
   
    // i will return only one, couse only one needed
    return $u;
}

function gauss_ms($m=0.0,$s=1.0)
{   // N(m,s)
    // returns random number with normal distribution:
    //   mean=m
    //   std dev=s
   
    return gauss()*$s+$m;
}

function random_0_1()
{   // auxiliary function
    // returns random number with flat distribution from 0 to 1
    return (float)rand()/(float)getrandmax();
}



