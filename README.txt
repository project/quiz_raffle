Quiz Raffle
===========

Quiz raffle module allows quiz admins to draw random winners from quiz
takers. Winners are picked using random permutations algorithm. You can
choose how many winners to generate with a minimum score.

Installation
============

Requires:
- CCK (userreference)
- Quiz

Download the module from http://drupal.org/project/quiz_raffle

Enable it. Edit user permissions and add respected roles permission to 'draw from results'.

Create some quizes, participate in them. Visit Quiz node and checkout Raffle tab.

Authors
=======

Module created by Jakub Suchy <jakub@dynamiteheads.com> for Dynamite Heads http://www.dynamiteheads.com

Random permutations algorithm created by X Y
