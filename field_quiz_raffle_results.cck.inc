<?php

$fields['field_quiz_raffle_results'] = array (
    'field_name' => 'field_quiz_raffle_results',
    'type_name' => 'quiz',
    'display_settings' => 
    array (
      'label' => 
      array (
        'format' => 'above',
        'exclude' => 0,
        ),
      'teaser' => 
      array (
        'format' => 'default',
        'exclude' => 0,
        ),
      'full' => 
      array (
        'format' => 'default',
        'exclude' => 0,
        ),
      4 => 
      array (
        'format' => 'default',
        'exclude' => 0,
        ),
      2 => 
      array (
          'format' => 'default',
          'exclude' => 0,
          ),
      3 => 
      array (
          'format' => 'default',
          'exclude' => 0,
          ),
      'token' => 
      array (
          'format' => 'default',
          'exclude' => 0,
          ),
      ),
      'widget_active' => '1',
      'type' => 'userreference',
      'required' => '0',
      'multiple' => '1',
      'db_storage' => '0',
      'module' => 'userreference',
      'active' => '1',
      'locked' => '0',
      'columns' => 
      array (
          'uid' => 
          array (
            'type' => 'int',
            'unsigned' => true,
            'not null' => false,
            ),
          ),
      'referenceable_roles' => 
      array (
          2 => 0,
          7 => 0,
          5 => 0,
          6 => 0,
          3 => 0,
          4 => 0,
          ),
      'referenceable_status' => 
      array (
          1 => 1,
          0 => 0,
          ),
      'advanced_view' => '--',
      'advanced_view_args' => '',
      'widget' => 
      array (
          'autocomplete_match' => 'contains',
          'size' => '30',
          'reverse_link' => 0,
          'default_value' => 
          array (
            0 => 
            array (
              'uid' => NULL,
              '_error_element' => 'default_value_widget][field_quiz_raffle_results][0][uid][uid',
              ),
            ),
          'default_value_php' => NULL,
          'label' => 'Quiz raffle',
          'weight' => '31',
          'description' => '',
          'type' => 'userreference_autocomplete',
          'module' => 'userreference',
          ),
      );
